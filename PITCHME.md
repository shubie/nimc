# SCALA
##### A SCAlable LAnguage Presentation
<br>

#### Shuaib Afegbua
---


### HISTORY

- Created By Martin Odersky in 2003 |
- A long time Java contributor |
- Ecole polytechnique fédérale de Lausanne (EPFL) in Lausanne, Switzerland |

---

### WHY SCALA ?

#### A programming language to make life easier and more productive for the developer. Based on three principles:

- Cuts down boilerplate |
- Expressive by tightly fusing object-oriented and functional programming concepts in one language |
- Built on top existing JVM infrastructure |

---
##### Java codebase
```java
package com.codekraft.abusivejava;

import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author shuaibafegbua
 */
public class Person {
    
    private String name;
    private String alias;
    private BigDecimal yams;

    public Person(String name, String alias, BigDecimal yams) {
        this.name = name;
        this.alias = alias;
        this.yams = yams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public BigDecimal getYams() {
        return yams;
    }

    public void setYams(BigDecimal yams) {
        this.yams = yams;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", alias=" + alias + ", yams=" + yams + '}';
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.alias);
        hash = 53 * hash + Objects.hashCode(this.yams);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.alias, other.alias)) {
            return false;
        }
        if (!Objects.equals(this.yams, other.yams)) {
            return false;
        }
        return true;
    }
    

    
}
```
---
### Scala

```scala
case class Person(var name: String, var alias: String, var yams: BigDecimal)
```
---

### Overall Design Goal

#### It aims at helping developers tackle new challenges like high-level domain modeling, rapid development, and, more recently, parallelism and concurrency

---

*Scala's unique combination of features also made it an excellent basis for concurrent, parallel, and distributed computing, which is widely thought to be the next big challenge in software development. Scala has a lot to offer in that space: from implicit parallelism in parallel collections to explicit concurrency using actors, it offers a toolset to write scalable concurrent applications simpler and with more confidence in their correctness.*



---

### Functional Programming
Functional programming is a way of writing software applications using
only pure functions and immutable values.
- Pure functions |
- Mutable |

---
### Pure functions
- The output of a pure function depends only on (a) its input parameters and
(b) its internal algorithm. |
- A pure function has no side effects, meaning that it does not read anything from the outside world or write anything to the outside world. |
- if a pure function is called with an input parameter x an infinite number of times, it will always return the same result y.|
---

### Benefits of Functional Programming

- Pure functions are easier to reason about |
- Testing is easier |
- Debugging is easier |
- Programs are written at a higher level easy to comprehend
- etc |

---

### Big Data

 Why Big Data


---
### Big Data and Scala

<br>

Why Big Data (Apache Spark) with Scala ?

---
### Language supported on Apache Spark

<br>

- Scala |
- Java |
- Python |
- R |

So why use Scala?

---
### why should use Scala for Apache Spark

<br>

- It's more productive working with Scala than with Java |
- Scala is faster than Python and R because it is compiled language |
- Scala is a functional language |


---

### Comparing Scala, Java, Python and R APIs in Apache Spark

![Image](assets/image/compare.png)

---


### LESSON ONE 

<br>
###### Installation and REPL

REPL - READ PRINT EVALUATION LOOP

---

### LESSON TWO 

<br>
Scala Data Types & Variable Types


---

### Definations

<br>

- Literals |
- Variables |
- Values |
- Types |

---

### Literals
```
4

'A'

"hello word"
```
---

### Variables
```
 var name = "Shuaib"

 var x = 2

 var grade = 'A'
```
---

### Values
```
 val name = "Shuaib"

 val x = 2

 val Var1 : String = "Kenny"

 val grade = 'A'

```

---

### Naming conventions

<br>

- A letter followed by zero or more letters and digits |

```

```

---

### Naming conventions

<br>

- A letter followed by zero or more letters and digits, then an underscore (_), and then one or more of either letters and digits or operator characters. |

```

```
---

### Naming conventions (Continuation)

<br>

- One or more operator characters and One or more of any character except a backquote, all enclosed in a pair of back- quotes |

```

```

---

### Types

<br>

- Numeric types: |
- Non numeric types: |
- Numeric literals: |
- Type conversion: |

---

### Scala Type Hierarchy

![Image](assets/image/scala-type.png)

---

### Strings

<br>

- string types: |
- Concatenation |
- String interpolation |

---

## LESSON THREE 

<br>
Basic Scala Operations: Arithmetic, Compiled Scala / Main:


---

### Arithmetic and Operator Overloading

<br>

- `+ - * / %` operators do their usual job |
- Operators in Scala are actually methods `a+b`  is equivalent `a.+(b)` |
- In general, you can write `a method b` as a shorthand for `a.method(b)` |

---

### Compiled Scala / Main

```scala
object HelloWorld {
  def main(args: Array[String]): Unit = {
    println("Hello, world!")
  }
}
```

---

### Compiled Scala / Main

```scala
object HelloWorld extends App {
  println("Hello, world!")
}
```
---

## LESSON FOUR 
Scala Expressions and Statement
<br>
```
val <identifier>[: <type>] = <expression>
```
- Expression refers to any block of code that returns a value |
- Expression block: One or more lines of code can be considered an expression if they are collected together using curly braces ({ and }) |
- Statement |

---

## LESSON FIVE 
If and If-Else Expressions

---
##### If expression

<br>
```
if (<Boolean expression>) <expression>`
```

```scala
 if ( 47 % 3 > 0 ) println("Not a multiple of 3")
```

##### If-Else expression

``` 
if (<Boolean expression>) <expression> else <expression>
```

```scala
 val x = 10; val y = 20
 val max = if (x > y) x else y
```
---

## LESSON SIX 
LOOPS 
- For loops |
- While and Do/While Loops |

---

##### For loops

Syntax: Defining a Numeric Range <br>
```
<starting integer> [to|until] <ending integer> [by increment]
```

Syntax: Iterating with a Basic For-Loop <br>
```
for (<identifier> <- <iterator>) [yield] [<expression>]
```

---

##### While and Do/While Loops

Syntax: A While Loop <br>
```
while (<Boolean expression>) statement
```

Syntax: do/while <br>
```
do  statement while (<Boolean expression>)

```


---

##### While and Do/While Loops

Example: A While Loop <br>
```
var x = 10; while (x > 0) x -= 1
```

Example: do/while <br>
```
 do println(s"Here I am, x = $x") while (x > 0)

```
---

## LESSON SEVEN 
MATCH EXPRESSIONS 
- Switch like constructs but more powerful |
- Matching with Wildcard Patterns : value binding and wildcard |
- Matching with Pattern Guards |

---

## LESSON EIGHT 
Scala User Input / Output:
- print() and printLn() |
- read functions: readLine, readInt,|


---

## EXERCISES


